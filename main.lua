love.graphics.setDefaultFilter("nearest", "nearest")
love.filesystem.setIdentity("rotmaker")

local spr = love.graphics.newImage("sprite.png")
local sw, sh = spr:getDimensions()
local deg = 0
local showsprite = false

love.window.setMode(sw*8, sh)

function love.update(dt)
	if showsprite then
		deg = deg+45
		if deg >= 360 then deg = 0 end
	end
end

function love.draw()
	love.graphics.clear(0, 0, 0, 0)
	love.graphics.setColor(1, 1, 1)
	if not showsprite then
		for i=0, 7 do
			deg = deg+45
			local rot = math.rad(deg)
			local xoff, yoff = sw / 2, sh / 2
			love.graphics.draw(spr, (i*sw)+sw/2, 0+sh/2,
				rot,
				1, 1,
				xoff,
				yoff
			)
		end
	else
		local rot = math.rad(deg)
		local xoff, yoff = sw / 2, sh / 2
		love.graphics.draw(spr,
			64+sw/2, 0+sh/2,
			rot,
			1, 1,
			xoff,
			yoff
		)
	end
end

function love.keypressed(key)
	if key == "c" then
		print("Click")
		love.graphics.captureScreenshot("screenshot.png")
	elseif key == "space" then
		deg = 0
		showsprite = not showsprite and true or false
	elseif key == "f" then
		love.system.openURL("file://" .. love.filesystem.getSaveDirectory())
	end
end