## Usage

1. Save your 1 frame sprite into `sprite.png`.
2. Run the app

Once inside, you'll see 8 frames of your sprite rotating. You can press `space` to play the animation to see what it looks like. `c` will click the image and capture a screenshot to the save directory, and `f` will open up the save directory in your file explorer.